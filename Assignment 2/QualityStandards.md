## List of Quality Standards for (Project Name)


| No. | Success Criteria/Requirement |	Quality standard|	Metric	|Quality Assurance |	Quality Control| Evaluation|
|--- |--- |	---| ---	|--- |	---| ---|
|1|	Students will apply general project management competencies, techniques related to IT project management|	There will be 50% marks allocated for the assignments in this learning outcome.	|Marks allocated per assignment	|The subject outline and assignments are checked by a moderator |	Results: Marks for assignment 1 are are 20% but assignment 2 are 25%, Action: to review mark allocations	| Quality standard not met: Need to remind moderator to check the mark allocation.|
						
